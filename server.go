package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/logantbond/ip-blocklist-api/db"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"gitlab.com/logantbond/ip-blocklist-api/api"
	"gitlab.com/logantbond/ip-blocklist-api/graph"
	"gitlab.com/logantbond/ip-blocklist-api/graph/generated"
)

const defaultPort = "8080"
const defaultDBFile = "./test/test.db"

func main() {

	dbFile := os.Getenv("DB_FILE") // allow custom db file
	if dbFile == "" {
		dbFile = defaultDBFile
	}

	// initialize the db connection
	conn, err := db.InitDB(dbFile)
	if err != nil {
		log.Fatalf("Unable to initialize the database connection: %s", err.Error())
	}
	defer conn.Close()

	log.Println("Building router and endpoints...")
	router := buildRouter(db.NewBlockListIPConnection(conn))

	walkFunc := func(
		method string,
		route string,
		handler http.Handler,
		middlewares ...func(http.Handler) http.Handler,
	) error {
		log.Printf("%s %s", method, route) // print out all methods/routes
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		log.Fatalf("Error walking the router methods/endpoints: %s", err.Error())
	}

	port := os.Getenv("PORT") // allow custom port
	if port == "" {
		port = defaultPort
	}

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: router,
	}

	// learned a nifty way to gracefully handle interrupts/sigint/sigterm
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() { // serve in goroutine
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Error serving: %s", err.Error())
		}
	}()
	log.Println("Server started!")

	log.Printf("Listening on port: %s", port)
	log.Printf("Navigate to /graphql in a browser for GraphQL playground!")

	<-done // grab the signal
	log.Println("Server stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err = server.Shutdown(ctx); err != nil {
		log.Fatalf("Server shutdown incorrectly! %s", err.Error())
	}

	// server shutdown gracefully
}

func buildRouter(conn db.BlockListIPConnection) *chi.Mux {
	r := chi.NewRouter()

	r.Use(
		render.SetContentType(render.ContentTypeJSON), // Set Content-Type headers to always JSON
		middleware.RequestID,                          // Injects a request ID into the context of each request
		middleware.RealIP,                             // Sets a http.Request's RemoteAddr to either X-Forwarded-For or X-Real-IP
		middleware.Logger,                             // Logs the start and end of each request with the elapsed processing time
		middleware.Recoverer,                          // Gracefully absorb panics and prints the stack trace
		middleware.Compress(5),                        // Gzip compression for clients that accept compressed responses
		middleware.RedirectSlashes,                    // Redirect slashes on routing paths
		middleware.BasicAuth("Basic Auth Realm", map[string]string{"secureworks": "supersecret"}),
	)

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.Use(middleware.Timeout(5 * time.Second))

	// GraphQL
	graphqlsrv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &graph.Resolver{DB: conn},
	}))

	r.Group(
		func(r chi.Router) {
			r.Mount("/", api.HealthCheckRouter())
			r.Mount("/graphql", playground.Handler("GraphQL playground", "/query"))
			r.Mount("/query", graphqlsrv)
		},
	)
	return r
}
