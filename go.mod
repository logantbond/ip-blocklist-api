module gitlab.com/logantbond/ip-blocklist-api

go 1.14

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/agnivade/levenshtein v1.1.0 // indirect
	github.com/dgryski/trifles v0.0.0-20200830180326-aaf60a07f6a3 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.2
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.5
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/net v0.0.0-20201202161906-c7110b5ffcbb // indirect
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
