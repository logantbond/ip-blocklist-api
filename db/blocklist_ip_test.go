package db

import (
	"database/sql"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/logantbond/ip-blocklist-api/graph/model"
)

// test case
var bip = &model.BlockListIP{
	UUID:         uuid.New().String(),
	CreatedAt:    time.Now().Format(time.RFC3339),
	UpdatedAt:    time.Now().Format(time.RFC3339),
	ResponseCode: "SBL - Spamhaus SBL Data",
	IPAddress:    "74.112.155.4",
}

func NewMockDB(t *testing.T) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock database connection: %s", err)
	}
	return db, mock
}

func TestFetchByIP(t *testing.T) {

	db, mock := NewMockDB(t)
	defer db.Close()

	testDB := NewBlockListIPConnection(db)

	query := "SELECT * FROM blocklist_ips WHERE ip_address = ?"
	rows := sqlmock.NewRows(
		[]string{"uuid", "created_at", "updated_at", "response_code", "ip_address"},
	).AddRow(bip.UUID, bip.CreatedAt, bip.UpdatedAt, bip.ResponseCode, bip.IPAddress)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(bip.IPAddress).WillReturnRows(rows)

	bip, err := testDB.FetchByIP(bip.IPAddress)
	assert.NotNil(t, bip)
	assert.NoError(t, err)
}

func TestFetchByIPErr(t *testing.T) {

	db, mock := NewMockDB(t)
	defer db.Close()

	testDB := NewBlockListIPConnection(db)

	query := "SELECT * FROM blocklist_ips WHERE ip_address = ?"
	rows := sqlmock.NewRows(
		[]string{"uuid", "created_at", "updated_at", "response_code", "ip_address"},
	)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(bip.IPAddress).WillReturnRows(rows)

	bip, err := testDB.FetchByIP(bip.IPAddress)
	assert.Empty(t, bip)
	assert.Error(t, err)
}

func TestCreate(t *testing.T) {

	db, mock := NewMockDB(t)
	defer db.Close()

	testDB := NewBlockListIPConnection(db)

	query := "INSERT INTO blocklist_ips(id, created_at, updated_at, response_code, ip_address) values(?,?,?,?,?)"

	prep := mock.ExpectPrepare(regexp.QuoteMeta(query))
	prep.ExpectExec().WithArgs(bip.UUID, bip.CreatedAt, bip.UpdatedAt, bip.ResponseCode, bip.IPAddress).WillReturnResult(sqlmock.NewResult(0, 1))

	err := testDB.Create(bip)
	assert.NoError(t, err)
}

func TestCreateErr(t *testing.T) {

	db, mock := NewMockDB(t)
	defer db.Close()

	testDB := NewBlockListIPConnection(db)

	query := "INSERT INTO blocklist_ips(id, created_at, updated_at, response_code, ip_address) values(?,?,?,?,?)"

	prep := mock.ExpectPrepare(regexp.QuoteMeta(query))
	prep.ExpectExec().WithArgs(bip.UUID, bip.CreatedAt, bip.UpdatedAt, bip.ResponseCode, "BOGUS IP").WillReturnResult(sqlmock.NewResult(0, 0))

	err := testDB.Create(bip)
	assert.Error(t, err)
}

func TestUpdate(t *testing.T) {

	db, mock := NewMockDB(t)
	defer db.Close()

	testDB := NewBlockListIPConnection(db)

	query := "UPDATE blocklist_ips SET updated_at=?, response_code=? WHERE ip_address=?"

	prep := mock.ExpectPrepare(regexp.QuoteMeta(query))
	prep.ExpectExec().WithArgs(bip.UpdatedAt, bip.ResponseCode, bip.IPAddress).WillReturnResult(sqlmock.NewResult(0, 1))

	err := testDB.Update(bip)
	assert.NoError(t, err)
}

func TestUpdateErr(t *testing.T) {

	db, mock := NewMockDB(t)
	defer db.Close()

	testDB := NewBlockListIPConnection(db)

	query := "UPDATE blocklist_ips SET updated_at=?, response_code=? WHERE ip_address=?"

	prep := mock.ExpectPrepare(regexp.QuoteMeta(query))
	prep.ExpectExec().WithArgs(bip.UpdatedAt, bip.ResponseCode, "BOGUS IP").WillReturnResult(sqlmock.NewResult(0, 0))

	err := testDB.Update(bip)
	assert.Error(t, err)
}
