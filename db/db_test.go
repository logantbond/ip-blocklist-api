package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitDB(t *testing.T) {

	db, err := InitDB("../test/test.db")
	defer db.Close()

	assert.NotNil(t, db)
	assert.NoError(t, err)
}
