package db

import (
	"database/sql"
	"log"
)

// InitDB takes a filepath as input and returns a *sql.DB using the sqlite3 driver
func InitDB(dbFile string) (*sql.DB, error) {

	log.Println("Opening DB file...")

	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		log.Printf("Error opening the specified database file: %s | Error text: %s", dbFile, err.Error())
		return nil, err
	}

	return db, nil
}
