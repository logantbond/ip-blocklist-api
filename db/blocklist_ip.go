package db

import (
	"context"
	"database/sql"
	"log"
	"time"

	_ "github.com/mattn/go-sqlite3" // sqlite library
	"gitlab.com/logantbond/ip-blocklist-api/api"
	"gitlab.com/logantbond/ip-blocklist-api/graph/model"
)

// BlockListIPConnection contains methods for database operations
type BlockListIPConnection interface {
	FetchByIP(string) (*model.BlockListIP, error)
	Create(*model.BlockListIP) error
	Update(*model.BlockListIP) error
}

type blocklistIPConnection struct {
	db *sql.DB
}

// NewBlockListIPConnection returns a connection handler with a *sql.DB
func NewBlockListIPConnection(db *sql.DB) BlockListIPConnection {
	return &blocklistIPConnection{db: db}
}

func (bipc *blocklistIPConnection) FetchByIP(ip string) (*model.BlockListIP, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	bip := &model.BlockListIP{}

	err := bipc.db.QueryRowContext(ctx, "SELECT * FROM blocklist_ips WHERE ip_address = ?", ip).Scan(&bip.UUID, &bip.CreatedAt, &bip.UpdatedAt, &bip.ResponseCode, &bip.IPAddress)
	if err != nil {
		return nil, err
	}

	return bip, nil

}

func (bipc *blocklistIPConnection) Create(bip *model.BlockListIP) error {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "INSERT INTO blocklist_ips(id, created_at, updated_at, response_code, ip_address) values(?,?,?,?,?)"

	stmt, err := bipc.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// don't care about result, just if it errored
	_, err = stmt.ExecContext(ctx, bip.UUID, bip.CreatedAt, bip.UpdatedAt, bip.ResponseCode, bip.IPAddress)

	return err
}

func (bipc *blocklistIPConnection) Update(bip *model.BlockListIP) error {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "UPDATE blocklist_ips SET updated_at=?, response_code=? WHERE ip_address=?"

	stmt, err := bipc.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// re-lookup IP
	newResponseCode, err := api.LookupIP(bip.IPAddress)
	if err != nil {
		return err
	}
	log.Printf("%s: updated response code - %s", bip.IPAddress, newResponseCode)

	// don't care about result, just if it errored
	_, err = stmt.ExecContext(ctx, time.Now().Format(time.RFC3339), newResponseCode, bip.IPAddress)

	return err
}
