FROM golang:1.15 as builder

ENV USER=user
ENV UID=10001 

WORKDIR /go/ip-blocklist-api

RUN apt update                  && \
    apt install git gcc         && \
    adduser                        \    
    --disabled-password            \    
    --gecos ""                     \    
    --home "/nonexistent"          \    
    --shell "/sbin/nologin"        \    
    --no-create-home               \    
    --uid "${UID}"                 \    
    "${USER}"

COPY go.* ./
RUN go mod download
COPY . .

RUN mkdir /db             && \
    mv ./test/test.db /db && \
    chown -R ${USER} /db  && \
    chmod -R a+rw /db

RUN GOOS=linux go build -a -ldflags="-extldflags=-static" -tags sqlite_omit_load_extension -o server server.go

FROM scratch

ENV PORT 8080
ENV DB_FILE /db/test.db

COPY --from=builder /go/ip-blocklist-api/server /server
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder --chown=10001:10001 /db /db

EXPOSE ${PORT}

USER user

CMD ["/server"]