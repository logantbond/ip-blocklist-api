package api

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
)

func TestHealtchCheckRouter(t *testing.T) {
	router := chi.NewRouter()
	router.Mount(
		"/",
		HealthCheckRouter(),
	)

	assert.NotNil(t, router)
}

func TestHealthCheck(t *testing.T) {

	rctx := chi.NewRouteContext()
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

	respRec := httptest.NewRecorder()

	router := chi.NewRouter()
	router.Get("/", healthCheck)
	router.ServeHTTP(
		respRec,
		req,
	)

	if status := respRec.Code; status != http.StatusOK {
		t.Errorf(
			"Test returned wrong status code! Got: %v | Want: %v",
			status,
			http.StatusOK,
		)
	}

	expected := `{"health":"I'm alive!"}`
	response := respRec.Body.String()
	if response != expected {
		t.Errorf(
			"Test returned unexpected respose body! Got: %v | Want: %v",
			response,
			expected,
		)
	}
}
