package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
)

// LookupIP takes in ip address, transforms it into reverse spamhaus format,
// and looks the ip up against spamhous.org using net.LookupHost()
func LookupIP(ipAddr string) (string, error) {

	spamHausURL := reverseIPandAddSpamhaus(ipAddr)
	results, err := net.LookupHost(spamHausURL)
	if err != nil {
		// check if error is not found error - which just means not on blocklist
		if err.(*net.DNSError).IsNotFound {
			return "IP not found on any Spamhaus blocklist", nil // return the error as the result if not found - "no host found"
		}
		// if some other error occurs, want to actually error
		return "", err
	}

	spamhausResponseCode := spamhausTableLookup(results[0])
	return spamhausResponseCode, nil
}

// ValidateIP takes an ip address as input and uses net.ParseIP to determine
// if the ip is formatted correctly
func ValidateIP(ipAddr string) error {
	ip := net.ParseIP(ipAddr)
	if ip == nil { // net.ParseIP returns nil if not an IP
		return fmt.Errorf("%s: invalid ip address", ipAddr)
	}
	return nil
}

func reverseIPandAddSpamhaus(ipAddr string) string {
	splitIP := strings.Split(ipAddr, ".")

	for i := 0; i < len(splitIP)/2; i++ {
		j := len(splitIP) - i - 1
		splitIP[i], splitIP[j] = splitIP[j], splitIP[i]
	}
	return fmt.Sprintf("%s.%s", strings.Join(splitIP, "."), "zen.spamhaus.org")
}

func spamhausTableLookup(responseCode string) string {

	translatedResponse := ""
	switch responseCode {
	case "127.0.0.2":
		translatedResponse = "SBL - Spamhaus SBL Data"
	case "127.0.0.3":
		translatedResponse = "SBL - Spamhaus SBL CSS Data"
	case "127.0.0.4":
		translatedResponse = "XBL - CBL Data"
	case "127.0.0.9":
		translatedResponse = "SBL - Spamhaus DROP/EDROP Data"
	case "127.0.0.10":
		translatedResponse = "PBL - ISP Maintained"
	case "127.0.0.11":
		translatedResponse = "PBL - Spamhaus Maintained"
	default:
		translatedResponse = fmt.Sprintf("%s: unknown response code", responseCode)
	}

	return translatedResponse
}

func respondWithJSON(w http.ResponseWriter, statusCode int, payload interface{}) error {
	log.Printf("Response: %v", payload)
	response, err := json.Marshal(payload)
	if err != nil {
		log.Printf("Unable to marshal JSON Response! %s", err.Error())
		return err
	}

	w.WriteHeader(statusCode)
	w.Write(response)

	return nil
}
