package api

import (
	"net/http"

	"github.com/go-chi/chi"
)

// HealthCheckRouter for simple healthcheck endpoint
func HealthCheckRouter() *chi.Mux {
	router := chi.NewRouter()

	// define routes
	router.Get("/", healthCheck)
	return router
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(
		w,
		http.StatusOK,
		map[string]string{"health": "I'm alive!"},
	)
}
