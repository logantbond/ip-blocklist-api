package api

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
)

func TestLookupIP(t *testing.T) {
	testCases := []struct {
		ipAddr   string
		expected string
		err      error
	}{
		{
			"99.13.2.35",
			"IP not found on any Spamhaus blocklist",
			nil,
		},
		{
			"74.112.155.4",
			"SBL - Spamhaus SBL Data",
			nil,
		},
	}

	for _, tc := range testCases {

		log.Printf("Testing input: %s", tc.ipAddr)

		spamhausResponse, err := LookupIP(tc.ipAddr)

		if err != nil && tc.err == nil {
			t.Errorf(
				"Expected a result, but got error instead! Got: '%v' | Want: '%v'",
				err,
				tc.expected,
			)
		} else if err == nil && tc.err != nil {
			t.Errorf(
				"Expected an error, but go result instead! Got: '%v' | Want: '%v'",
				err,
				tc.err,
			)
		} else if err != nil && tc.err != nil {
			if err.Error() != tc.err.Error() {
				t.Errorf(
					"Expected an error, but got the incorrect error! Got: '%v' | Want: '%v'",
					err,
					tc.err,
				)
			}
		}

		if spamhausResponse != tc.expected {
			t.Errorf(
				"Test IP lookup returned wrong response! Got: '%v' | Want: '%v'",
				spamhausResponse,
				tc.expected,
			)
		}
	}
}

func TestValidateIP(t *testing.T) {
	testCases := []struct {
		ipAddr   string
		expected error
	}{
		{
			"74.112.155.4",
			nil,
		},
		{
			"123test",
			fmt.Errorf("%s: invalid ip address", "123test"),
		},

		{
			"543543.54335.4535.243",
			fmt.Errorf("%s: invalid ip address", "543543.54335.4535.243"),
		},
	}

	for _, tc := range testCases {

		log.Printf("Testing input: %s", tc.ipAddr)

		err := ValidateIP(tc.ipAddr)

		if err != nil && err.Error() != tc.expected.Error() {
			t.Errorf(
				"Test returned incorrect error! Got: '%v' | Want: '%v'",
				err,
				tc.expected,
			)
		}
	}
}

func TestReverseIPandAddSpamhaus(t *testing.T) {
	testCases := []struct {
		ipAddr   string
		expected string
	}{
		{
			"99.13.2.35",
			"35.2.13.99.zen.spamhaus.org",
		},
		{
			"74.112.155.4",
			"4.155.112.74.zen.spamhaus.org",
		},
	}

	for _, tc := range testCases {

		log.Printf("Testing input: %s", tc.ipAddr)

		spamhausURL := reverseIPandAddSpamhaus(tc.ipAddr)

		if spamhausURL != tc.expected {
			t.Errorf(
				"Test returned the wrong result! Got: '%v' | Want: '%v'",
				spamhausURL,
				tc.expected,
			)
		}
	}
}

func TestSpamhausTableLookup(t *testing.T) {
	testCases := []struct {
		responseCode string
		expected     string
	}{
		{
			"127.0.0.2",
			"SBL - Spamhaus SBL Data",
		},
		{
			"127.0.0.3",
			"SBL - Spamhaus SBL CSS Data",
		},
		{
			"127.0.0.4",
			"XBL - CBL Data",
		},
		{
			"127.0.0.9",
			"SBL - Spamhaus DROP/EDROP Data",
		},
		{
			"127.0.0.10",
			"PBL - ISP Maintained",
		},
		{
			"127.0.0.11",
			"PBL - Spamhaus Maintained",
		},
		{
			"jfkdslfjdskflja",
			"jfkdslfjdskflja: unknown response code",
		},
	}

	for _, tc := range testCases {

		log.Printf("Testing input: %s", tc.responseCode)

		translatedResponse := spamhausTableLookup(tc.responseCode)

		if translatedResponse != tc.expected {
			t.Errorf(
				"Test returned incorrect translated response code! Got: '%v' | Want: '%v'",
				translatedResponse,
				tc.expected,
			)
		}
	}
}

func TestRespondWithJSON(t *testing.T) {

	rctx := chi.NewRouteContext()
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

	respRec := httptest.NewRecorder()

	router := chi.NewRouter()
	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		respondWithJSON(
			w,
			http.StatusOK,
			map[string]string{"key": "value"},
		)
	})
	router.ServeHTTP(
		respRec,
		req,
	)

	if status := respRec.Code; status != http.StatusOK {
		t.Errorf(
			"Test returned wrong status code! Got: '%v' | Want: '%v'",
			status,
			http.StatusOK,
		)
	}

	expected := `{"key":"value"}`
	response := respRec.Body.String()
	if response != expected {
		t.Errorf(
			"Test returned unexpected respose body! Got: '%v' | Want: '%v'",
			response,
			expected,
		)
	}

	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		respondWithJSON(
			w,
			http.StatusTooManyRequests,
			make(chan int),
		)
	})

	errReq, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	errReq = errReq.WithContext(context.WithValue(errReq.Context(), chi.RouteCtxKey, rctx))

	errRespRec := httptest.NewRecorder()
	router.ServeHTTP(
		errRespRec,
		errReq,
	)

	assert.Equal(t, "", errRespRec.Body.String())

}
