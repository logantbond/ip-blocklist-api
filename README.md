# IP Blocklist API Challenge

Product management just came back from a security conference and saw people using free
threat intel sources to do detections. Using DNS requests, you can send an IP address in
reverse notation to determine whether it is on a block list or not.

Naturally, they want us to be able to query these servers and provide answers to our customers
on whether or not an IP is malicious.

Given an IP address of 1.2.3.4 we can look that up at Spamhause by issuing the following
commands (just an example):
```bash
host 4.3.2.1.zen.spamhaus.org
```
or
```bash
dig 4.3.2.1.zen.spamhaus.org
```
#### [Spamhaus Response Codes](https://www.spamhaus.org/faq/section/DNSBL%20Usage#200)

## Overview

The goal of the IP BlockList API Challenge is to accept a list of IP addresses, check each one against Spamhaus blocklists, and catalog each one in a database. If an IP has already been checked and cataloged, re-lookup the IP against Spamhaus to update the Spamhaus response and log the time of the update.

#### Tech Stack:
* Golang API with GraphQL (using `go.mod` and `go.sum`)
* SQLite DB 
* Dockerfile: [link to registry](https://gitlab.com/logantbond/ip-blocklist-api/container_registry)
* Helm chart for kubernetes deployment
* Gitlab CICD

#### Golang External Libraries Used
* [`github.com/go-chi/chi`](https://github.com/go-chi/chi) - extremely flexible router framework for an API that makes things like middleware extremely easy. It has less 1000 LOC and is 100% compatible with `net/http`
* [`github.com/google/uuid`](https://github.com/google/uuid) - based on the RFC, built by Google, simple UUID generator that I could easily trust
* [`github.com/mattn/go-sqlite3`](https://github.com/mattn/go-sqlite3) -  didn't want to use an ORM for something that can remain simple, and this was the most popular sqlite3 driver I could find
* [`github.com/99designs/gqlgen/graphql`](https://gqlgen.com) - for GraphQL Golang integration
* [`github.com/DATA-DOG/go-sqlmock`](https://github.com/DATA-DOG/go-sqlmock) - testing the database functions a bit more properly

## Running Locally:

**Once up and running, visit http://localhost:8080/graphql to get into the GraphQL Playground!**

#### Using the `Makefile`

The best way to get started is probably with the `Makefile`. It is probably easiest to just open the `Makefile` and read the commands it will run, but here are some helpful targets:

* `make all`: download dependencies, run tests (with coverage output), and compile with `go build`
* `make deps`: download dependencies
* `make test`: run tests with coverage output
* `make test-html`: run tests with html coverage output
* `make build`: compile the application
* `make run`: runs the server
* `make docker`: build and run the Dockerfile with all defaults
* `make docker-newdb`: creates a new db, initializes it, builds the container, and runs the container with the new db mounted from `$(pwd)`
* `make deploy`: deploy the helm chart with auto-generated name
* `make clean`: cleans go caching
* `make clean-all`: *destructive!* - cleans go caching, any containers, and *any db created from the Makefile* 

#### Cloning from Source:

By default, the repo provides a small database in `./test/test.db`, which the application will look to by default.
```bash
# clone the repo and cd into it
╰─ git clone https://gitlab.com/logantbond/ip-blocklist-api.git
Cloning into 'ip-blocklist-api'...
remote: Enumerating objects: 37, done.
remote: Counting objects: 100% (37/37), done.
remote: Compressing objects: 100% (35/35), done.
remote: Total 115 (delta 8), reused 23 (delta 0), pack-reused 78
Receiving objects: 100% (115/115), 45.96 KiB | 1.31 MiB/s, done.
Resolving deltas: 100% (36/36), done.

╰─ cd ip-blocklist-api/

# install dependencies
╰─ go get -d -v ./...

# run the app
╰─ go run server.go 
```

##### Using your own DB:
If you'd like to use your own DB, it's relatively simple to setup a new sqlite DB:

```bash
# create the db
╰─ touch ip.db

# open sqlite3
╰─ sqlite3 
SQLite version 3.32.3 2020-06-18 14:16:19
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.
sqlite> .open ip.db
sqlite> CREATE TABLE `blocklist_ips` (
   ...>     `id` STRING PRIMARY KEY,
   ...>     `created_at` TEXT,
   ...>     `updated_at` TEXT,
   ...>     `response_code` VARCHAR(100),
   ...>     `ip_address` VARCHAR(16)
   ...> );
```

For easy copy/paste, here's the `CREATE TABLE` sql:

```sql
CREATE TABLE `blocklist_ips` (
    `id` STRING PRIMARY KEY,
    `created_at` TEXT,
    `updated_at` TEXT,
    `response_code` VARCHAR(100),
    `ip_address` VARCHAR(16)
);
```

The application will look to the environment for a db file if the environment variable `DB_FILE` is set:
```bash
╰─ export DB_FILE=/path/to/my/db/file
```

#### Running with Docker

A Dockerfile is provided in the root of the repo. By default, it exposes port 8080.

```bash
╰─ docker build -t ip-blocklist-api:latest .
# build output
# ...
# ...
Successfully built a25d09c323f4
Successfully tagged ip-blocklist-api:latest

╰─ docker run -d --name ip-blocklist-api --restart unless-stopped -p 8080:8080 ip-blocklist-api:latest
```

Example running with your own database (requires a volume mount to your own database - *note: mount your db somewhere under /db in the container*):
```bash
╰─ docker run -d --name ip-blocklist-api --restart unless-stopped -p 8080:8080 -v $(pwd)/ip.db:/db/ip.db -e DB_FILE=/db/ip.db ip-blocklist-api:latest 
```

Or just run it directly from the registry:
```bash
# master:latest can be assumed as stable
# will require a gitlab account!
╰─ docker run -d --name ip-blocklist-api --restart unless-stopped -p 8080:8080 registry.gitlab.com/logantbond/ip-blocklist-api/master:latest
```

#### Running with Helm

1. Have a kube cluster: (I'd recommend [k3s](https://k3s.io/) for something simple to test)
2. Download [`kubectl`](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and ensure you have access to your cluster (k3s will do this for you)
3. Download [`helm`](https://helm.sh/docs/intro/install/) `> v3.0`

By default, the repo provides a helm chart in `/deploy`, which contains a `values.yaml` to get started. The `values.yaml` file is good to be left as default for getting started quickly/easily.

```bash
# this will generate a random name
╰─ helm install ./deploy --generate-name 
NAME: deploy-1606936995
LAST DEPLOYED: Wed Dec  2 13:23:17 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
1. Get the application URL by running these commands:
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=ip-blocklist-api,app.kubernetes.io/instance=deploy-1606936995" -o jsonpath="{.items[0].metadata.name}")
  kubectl --namespace default port-forward $POD_NAME 8080:8080

  Visit http://localhost:8080/graphql to use your application

# running the commands from the above output notes
╰─ export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=ip-blocklist-api,app.kubernetes.io/instance=deploy-1606936995" -o jsonpath="{.items[0].metadata.name}")

╰─ kubectl --namespace default port-forward $POD_NAME 8080:8080
```

## GraphQL

A GraphQL Playground endpoint is provided with the application at `/graphql`. For testing, a mutation and query are provided. The test data included is my personal IP (please don't DDOS me :slight_smile:), and a known spam IP address registered with Spamhaus (took me a while to dig that up).

```graphql
mutation enqueue {
  enqueue(input:["99.13.2.35", "74.112.155.4"]) {
    uuid
    created_at
    updated_at
    response_code
    ip_address
  }
}

query getIPDetails {
  getIPDetails(input: "74.112.155.4") {
    uuid
    created_at
    updated_at
    response_code
    ip_address
  }
}
```

The current version of the schema can be found in [`graph/shema.graphqls`](https://gitlab.com/logantbond/ip-blocklist-api/-/blob/master/graph/schema.graphqls)

## FAQ

#### Tests fail (and/or are slow) with 74.112.155.4!

I had to spend a bit digging up an IP address that was registered with Spamhous. This particular IP was mentioned on their site in URL form (I just did an `nslookup`). However, I noticed that on Google's DNS `8.8.8.8`, that IP didn't resolve with a Spamhaus lookup. Personally, I have my home net setup with Cloudflare's `1.1.1.3/1.0.0.3`, and it consistently resolves there.

The slowness in the tests may be due to slow DNS lookups (testing looking up an IP), and failures may be due to inability to resolve the above, known-spam testing IP.

I had to workaround this in the pipeline as well - see [`.gitlab-ci.yml`](https://gitlab.com/logantbond/ip-blocklist-api/-/blob/master/.gitlab-ci.yml)

#### There are a lot of files in this helm `deploy/` repo...

To make my life easier and not over-engineer things, I actually used `helm create deploy` to generate the base helm chart files. There is definitely more in there than needed to run this small application, but I wanted to start from their sane defaults. Beyond that, not much was customized aside from the default service port (8080), the readiness/liveness probes (to account for basic authentication), and the `NOTES.txt` for custom output.

#### API uses basic auth? What's the default user/pass?

user: `secureworks`\
pass: `supersecret`

#### Project Layout/Folder Structure

Project layout follows https://github.com/golang-standards/project-layout with a few exceptions:

* `Dockerfile`: A Dockerfile is one of the easiest ways to learn how a project is built/packaged. So I prefer to keep it easily accessible in the root of the project rather than in `build/`.
* `.gitlab-ci.yml`: Same reasoning as the Dockerfile - a pipeline is one of the easiest ways to learn how a project is tested/linted/built/deployed. So I prefer to keep it easily accessible in the root of the project rather than in `/build/ci/` (plus it's Gitlab's default). 

#### Testing Helm

To test the helm deployment, I setup my own Gitlab shell executor that had access to a k3s cluster, `kubectl` and `helm`. I did not port forward to expose the app running in the kube cluster publicly, but I did use Gitlab CI to deploy out of the helm chart each commit. Pipeline logs showing successful deployments can be [found here](https://gitlab.com/logantbond/ip-blocklist-api/-/pipelines).