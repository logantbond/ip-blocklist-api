package graph

//go:generate go run github.com/99designs/gqlgen

import (
	"gitlab.com/logantbond/ip-blocklist-api/db"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

// Resolver is a struct for maintaining state between GraphQL and Golang
// This struct was automatically generated with gqlgen
type Resolver struct {
	DB db.BlockListIPConnection
}
