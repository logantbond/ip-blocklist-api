package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/logantbond/ip-blocklist-api/api"
	"gitlab.com/logantbond/ip-blocklist-api/graph/generated"
	"gitlab.com/logantbond/ip-blocklist-api/graph/model"
)

func (r *mutationResolver) Enqueue(ctx context.Context, input []string) ([]*model.BlockListIP, error) {

	log.Printf("Received input: %v", input)
	bips := make([]*model.BlockListIP, 0)

	for _, ip := range input {

		log.Printf("Handling ip: %s", ip)

		// Validate the IP before doing anything
		if err := api.ValidateIP(ip); err != nil {
			log.Printf("Invalid ip! %s: %s", ip, err.Error())
		} else {

			bip, err := r.DB.FetchByIP(ip)
			if err != nil {

				log.Printf("%s: ip does not already exist in db", ip)

				response, err := api.LookupIP(ip)
				if err != nil {
					log.Println(err.Error())
					return nil, err
				}

				bip = &model.BlockListIP{
					UUID:         uuid.New().String(),
					CreatedAt:    time.Now().Format(time.RFC3339),
					UpdatedAt:    time.Now().Format(time.RFC3339),
					ResponseCode: response,
					IPAddress:    ip,
				}

				err = r.DB.Create(bip)
				if err != nil {
					log.Println(err.Error())
					return nil, err
				}

			} else { // IP already exists

				log.Printf("%s: ip already exists in database", ip)

				err = r.DB.Update(bip)
				if err != nil {
					log.Println(err.Error())
					return nil, err
				}

			}
			bips = append(bips, bip)
		}
	}

	return bips, nil
}

func (r *queryResolver) GetIPDetails(ctx context.Context, input string) (*model.BlockListIP, error) {

	// Validate the IP before doing anything
	if err := api.ValidateIP(input); err != nil {
		log.Printf("Invalid ip! %s: %s", input, err.Error())
		return nil, err
	}

	bip, err := r.DB.FetchByIP(input)
	if err != nil {
		log.Printf("%s: %s", input, err.Error())
		return nil, err
	}

	return bip, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
