UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
OS := darwin
else
OS := linux
endif

build:
	GOOS=$(OS) go build -a -o ip-blocklist-api server.go
	chmod +x ip-blocklist-api

run:
	go run server.go

deps:
	go mod download

lint:
	golint -set_exit_status $(shell go list ./... | grep -v /vendor/)

.PHONY: test
test:
	go test -race $$(go list ./... | grep -v /vendor/) -v -coverprofile=coverage.out
	go tool cover -func=coverage.out

test-html:
	go test -race $$(go list ./... | grep -v /vendor/) -v -coverprofile=coverage.out
	go tool cover -html=coverage.out

tidy:
	go mod tidy

.PHONY: deploy
deploy:
	helm install ./deploy --generate-name

clean:
	go clean -cache
	go clean -testcache

clean-all:
	go clean -cache
	go clean -testcache
	docker stop ip-blocklist-api || true
	docker rm ip-blocklist-api || true
	find . -name ip.db -exec rm {} +

# Docker
docker:
	docker build -t ip-blocklist-api .
	docker run -d --name ip-blocklist-api --restart unless-stopped -p 8080:8080 ip-blocklist-api

docker-newdb:
	touch ip.db
	chmod 777 ip.db
	sqlite3 ip.db "CREATE TABLE 'blocklist_ips' ('id' STRING PRIMARY KEY, 'created_at' TEXT, 'updated_at' TEXT, 'response_code' VARCHAR(100), 'ip_address' VARCHAR(16));"
	docker build -t ip-blocklist-api .
	docker run -d --name ip-blocklist-api --restart unless-stopped -p 8080:8080 -v $$(pwd)/ip.db:/db/ip.db -e DB_FILE=/db/ip.db ip-blocklist-api

docker-clean:
	docker stop ip-blocklist-api || true
	docker rm ip-blocklist-api || true

all: deps test build